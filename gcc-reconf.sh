#!/bin/sh -e
export PATH="$HOME/opt/autoconf-2.64/bin:$PATH"
export PATH="$HOME/opt/automake-1.11.6/bin:$PATH"
echo autogen Makefile.def
autogen Makefile.def
GCC_M4_DIR=$PWD/config
find ./ -name configure.ac | \
grep -v /zlib/contrib/minizip/ | \
while read filepath; do
  echo "(cd $(dirname "$filepath") && autoreconf -I "$GCC_M4_DIR")"
  (cd "$(dirname "$filepath")"/ &&
    (autoreconf -I "$GCC_M4_DIR" ||
     (libtoolize || true) &&
     (echo "Got an error, trying again:";
      autoreconf -I "$GCC_M4_DIR")))
done
find ./ -type d -name autom4te.cache | while read filepath; do
  rm -rf "$filepath"
done
#DATEFILE=$(mktemp)
#echo 'touch $(find)'
#find | grep -Ev '^\./\.git' | while read FILE; do touch -r "$DATEFILE" "$FILE"; done
#rm "$DATEFILE"
#echo "$0: done"
