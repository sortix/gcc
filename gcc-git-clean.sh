#!/bin/sh -e

if [ -f Makefile ]; then
  make distclean
fi

find_all_files() {
  find ./ -type f
}

find_files() {
  find ./ -type f -name "$1"
}

find_dirs() {
  find ./ -type d -name "$1"
}

remove_dirs() {
  while read filepath; do
    echo rm -r \'"$filepath"\' >&2
    rm -r "$filepath"
  done
}

remove_files() {
  while read filepath; do
    echo rm \'"$filepath"\' >&2
    rm "$filepath"
  done
}

remove_dirs_called() {
  find_dirs "$1" | remove_dirs
}

remove_files_called() {
  find_files "$1" | remove_files
}

only_if_have_makefile_am() {
  while read filepath; do
    if [ -e "$(dirname "$filepath")/Makefile.am" ]; then
      echo "$filepath"
    else
      echo '#' Not deleting \'"$filepath"\' >&2
    fi
  done
}

only_if_generated_aclocal() {
  while read filepath; do
    if head -1 "$filepath" | grep -q 'generated automatically by aclocal'; then
      echo "$filepath"
    else
      echo '#' Not deleting \'"$filepath"\' >&2
    fi
  done
}

find_all_files | grep '~$' | remove_files
remove_dirs_called autom4te.cache
remove_files_called configure
find_files aclocal.m4 | only_if_generated_aclocal | remove_files
remove_files_called config.in
remove_files_called config.h.in
remove_files_called gc_config.h.in
find_files Makefile.in | only_if_have_makefile_am | remove_files
