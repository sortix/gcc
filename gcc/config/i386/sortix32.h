/* Copyright(C) Jonas 'Sortie' Termansen 2012, 2013, 2014, 2015. */
/* Insert regular GCC licence information here with proper exceptions. */

/* TODO: Remove this after releasing Sortix 1.0 when compatibility with
         Sortix 0.9 is no longer required. */
/* Search for crt*.o files in this directory. */
#undef STANDARD_STARTFILE_PREFIX_1
#define STANDARD_STARTFILE_PREFIX_1 "/i486-sortix/lib/"

/* Use the stack frame linked list by default (-fno-omit-frame-pointer). */
#undef USE_IX86_FRAME_POINTER
#define USE_IX86_FRAME_POINTER 1

/* HACK: This currently causes trouble on Sortix, so disable it by default.
         (-fno-asynchronous-unwind-tables) */
#define SUBTARGET_OVERRIDE_OPTIONS \
	do { \
		flag_asynchronous_unwind_tables = 0; \
	} while (0);

/* Additional linker options. */
#define LINK_SPEC "%{m32:-m elf_i386_sortix} %{m64:-m elf_x86_64_sortix}"
