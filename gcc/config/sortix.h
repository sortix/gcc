/* Copyright(C) Jonas 'Sortie' Termansen 2012, 2013, 2014, 2015. */
/* Insert regular GCC licence information here with proper exceptions. */

/* The Sortix standard library does what glibc does, so copied from gnu-user. */

#undef TARGET_SORTIX
#define TARGET_SORTIX 1

/* Don't automatically add extern "C" { } around header files.  */
#undef  NO_IMPLICIT_EXTERN_C
#define NO_IMPLICIT_EXTERN_C 1

/* Objects automatically prepended to the link command line. */
#undef  STARTFILE_SPEC
#define STARTFILE_SPEC "crt0.o%s crti.o%s crtbegin.o%s"

/* Objects automatically appended to the link link command line. */
#undef  ENDFILE_SPEC
#define ENDFILE_SPEC "crtend.o%s crtn.o%s"

/* Search for crt*.o files in this directory. */
#undef STANDARD_STARTFILE_PREFIX
#define STANDARD_STARTFILE_PREFIX "/lib/"

/* Handle the mutual dependencies between libpthread, libm and libc. */
#undef LIB_SPEC
#define LIB_SPEC "--start-group %{pthread:}-lpthread -lm -lc --end-group"

/* ubsan is provided by Sortix libc. */
#undef LIBUBSAN_SPEC
#define LIBUBSAN_SPEC ""

/* Define relevant macros for Sortix. */
#undef  TARGET_OS_CPP_BUILTINS
#define TARGET_OS_CPP_BUILTINS()   \
do {                               \
  builtin_define("__sortix__");    \
  builtin_define("__unix__");      \
  builtin_assert("system=sortix"); \
  builtin_assert("system=unix");   \
  builtin_assert("system=posix");  \
} while(0);

/* ABI decisions. */
#undef PID_TYPE
#define PID_TYPE INTPTR_TYPE

/* Provide __SIZE_TYPE__ and __SSIZE_TYPE__ declarations. */
#undef SIZE_TYPE
#undef SSIZE_TYPE
#define SIZE_TYPE (TARGET_64BIT ? "long unsigned int" : "unsigned int")
#define SSIZE_TYPE (TARGET_64BIT ? "long signed int" : "signed int")

/* Fix PTRDIFF_TYPE being long int even if -m32 */
#undef PTRDIFF_TYPE
#define PTRDIFF_TYPE (TARGET_64BIT ? "long int" : "int")
