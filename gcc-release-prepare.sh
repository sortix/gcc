#!/bin/sh -e
./gcc-reconf.sh
./configure --disable-bootstrap
${MAKE-make} -k -j8 || true
${MAKE-make} -k distclean || true

find_all_files() {
  find ./ -type f
}

remove_files() {
  while read filepath; do
    echo rm \'"$filepath"\' >&2
    rm "$filepath"
  done
}

find_dirs() {
  find ./ -type d -name "$1"
}

remove_dirs() {
  while read filepath; do
    echo rm -r \'"$filepath"\' >&2
    rm -r "$filepath"
  done
}

remove_dirs_called() {
  find_dirs "$1" | remove_dirs
}

find_all_files | grep -E '\.Po$' | remove_files
find_all_files | grep -E '\~$' | remove_files
remove_dirs_called '.deps'
rm -rf gcc/build
rm -rf 'libgcc/$libgcc_topdir'
rm -rf 'libiberty/$libiberty_topdir'
