#!/bin/sh -e
if [ $# -lt 1 ]; then
  echo "$0: no destination directory for the detached libstdc++ given"
  exit 1
fi

stat libstdc++-v3 > /dev/null
stat gcc > /dev/null

DEST="$1"

if [ -f "$DEST/DETACHED_LIBSTDC++" ]; then
  rm -rf -- "$DEST"
fi

mkdir -v "$DEST"

echo "\`libstdc++-v3/.' -> \`$DEST'"
cp -R --preserve=all libstdc++-v3/. -t "$DEST"

echo "Creating $DEST/DETACHED_LIBSTDC++"
touch "$DEST/DETACHED_LIBSTDC++"

cp -v --preserve=all install-sh -t "$DEST"
cp -v --preserve=all config.sub -t "$DEST"
cp -v --preserve=all config.guess -t "$DEST"
cp -v --preserve=all config-ml.in -t "$DEST"
cp -v --preserve=all ltmain.sh -t "$DEST"
cp -v --preserve=all mkinstalldirs -t "$DEST"
cp -v --preserve=all missing -t "$DEST"

echo "Fixing $DEST/\`libstdc++-v3/configure'"
sed -i 's/multi_basedir=\"$srcdir.*/multi_basedir=\"${srcdir}\"/g' "$DEST/configure"
sed -i 's/toplevel_builddir=${glibcxx_builddir}\/../toplevel_builddir=${glibcxx_builddir}/' "$DEST/configure"
sed -i 's/toplevel_srcdir=${glibcxx_srcdir}\/../toplevel_srcdir=${glibcxx_srcdir}/' "$DEST/configure"

mkdir -pv "$DEST/gcc"
cp -v gcc/BASE-VER "$DEST/gcc"
cp -v gcc/DATESTAMP "$DEST/gcc"

mkdir -pv "$DEST/libgcc"
cp -v libgcc/*.h -t "$DEST/libgcc"

mkdir -pv "$DEST/libiberty"
cp -v libiberty/cp-demangle.c -t "$DEST/libiberty"
cp -v libiberty/cp-demangle.h -t "$DEST/libiberty"

mkdir -pv "$DEST/include"
cp -v include/ansidecl.h -t "$DEST/include"
cp -v include/libiberty.h -t "$DEST/include"
cp -v include/demangle.h -t "$DEST/include"

echo "Fixing \`$DEST/*/Makefile.in'"
sed -i 's/'\
'gcc_version := $(shell cat $(top_srcdir)\/..\/gcc\/BASE-VER)/'\
'gcc_version := $(shell cat $(top_srcdir)\/gcc\/BASE-VER)/' $(find "$DEST" -name Makefile.in)
